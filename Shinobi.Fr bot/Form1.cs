﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Shinobi.Fr_bot
{
    public partial class botForm : Form
    {
        public botForm()
        {
            InitializeComponent();
            botPanel.Visible = false;
            loginPanel.Visible = true;
        }

        

        private void hpLimitNums_ValueChanged(object sender, EventArgs e)
        {
            hpLimit2.Minimum = hpLimit.Value;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (loginPanel.Visible == true)
            {
                try
                {
                    //start browser
                    var serviceChrome = ChromeDriverService.CreateDefaultService();
                    serviceChrome.HideCommandPromptWindow = true;
                    serviceChrome.SuppressInitialDiagnosticInformation = true;
                    var optionsChrome = new ChromeOptions();
                    optionsChrome.AddArguments("incognito", "--start-maximized");
                    //  optionsChrome.AddAdditionalCapability("binary", "./chrome");
                    optionsChrome.AddExcludedArgument("enable-automation");
                    webDriver = new ChromeDriver(serviceChrome, optionsChrome);
                    //login
                    webDriver.Navigate().GoToUrl(@"http://www.shinobi.fr/");
                    webDriver.FindElement(By.Id("login")).SendKeys(loginInput.Text + OpenQA.Selenium.Keys.Tab
                                                                     + passwordInput.Text + OpenQA.Selenium.Keys.Enter);
                    //switch application panels
                    botPanel.Visible = true;
                    loginPanel.Visible = false;
                    //find stats --- Long
                    getStats();
                }
                catch (NoSuchElementException)
                {
                    webDriver.Quit();
                    MessageBox.Show("Website Problem");
                    this.Dispose(true);
                }

            }

        }

        private void toggleOn_Click(object sender, EventArgs e)
        {
            Thread bot = new Thread( () => runBot());
            bot.Start();
            //Task.Run(() => runBot());
        }

        public void runBot()
        {
            //InvokeDelagate ok = updateCharBox;
            while (ToggleOn.Checked)
            {
                //IAlert alert = webDriver.SwitchTo().Alert();
                if (!isAlertPresent() && getStats())
                {
                    //getStats();
                    //updateCharBox();
                    try
                    {
                        Random r = new Random();
                        if (Convert.ToInt32(hp.Text) < hpLimit.Value)
                        {
                            webDriver.FindElement(By.TagName("body")).SendKeys(keysToSend.Text);
                         //   Thread.Sleep(r.Next(1, 2));
                            webDriver.FindElement(By.TagName("body")).SendKeys("w");
                          //  webDriver.FindElement(By.TagName("body")).SendKeys("x");
                          //  _ = getStats();
                        }
                        if (Convert.ToInt32(hp.Text) >= hpLimit2.Value
                            && Convert.ToInt32(hp.Text) < Convert.ToInt32(hpMax.Text))
                        {
                            webDriver.FindElement(By.TagName("body")).SendKeys(keysToSend2.Text);
                         //   Thread.Sleep(r.Next(1, 2));
                            webDriver.FindElement(By.TagName("body")).SendKeys("w");
                          //  webDriver.FindElement(By.TagName("body")).SendKeys("x");
                        //    _ = getStats();
                        }
                        if (Convert.ToInt32(hp.Text) < HpLimit3.Value)
                        {
                            webDriver.FindElement(By.TagName("body")).SendKeys(KeyToSend3.Text);
                            //Thread.Sleep(r.Next(1, 2));
                            webDriver.FindElement(By.TagName("body")).SendKeys("w");
                       //     _ = getStats();
                        }
                        if (Convert.ToInt32(hp.Text) < HpLimit4.Value)
                        {
                            webDriver.FindElement(By.TagName("body")).SendKeys(KeyToSend4.Text);
                          //  Thread.Sleep(r.Next(1, 2));
                            webDriver.FindElement(By.TagName("body")).SendKeys("w");
                        //    _ = getStats();
                        }
                        if (fuyardToggle.Checked == true)
                        {
                            if (selectedAttitude.Text.Contains("Fuyarde"))
                            {
                           //     Thread.Sleep(r.Next(1, 5));
                                webDriver.FindElement(By.TagName("body")).SendKeys("r");
                            }
                        }

                      //  Thread.Sleep(r.Next(1, 5));
                    //    checkBreak();
                       // Thread.Sleep(r.Next(1, 5));
                        ReEquip();

                        _ = charInfoBox.BeginInvoke(new InvokeDelagate(updateCharBox));

                        Thread.Sleep(r.Next((int)minDelayNum.Value, (int)maxDelayNum.Value));
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }
        }

        public delegate void InvokeDelagate();
        public bool getStats()
        {
            try
            {
                charName = webDriver.FindElement
                    (By.CssSelector("#blocPersonnage > legend"));
                hp = webDriver.FindElement
                        (By.CssSelector(".js-life"));
                hpMax = webDriver.FindElement
                    (By.CssSelector(".js-life-max"));
                //level = webDriver.FindElement(By.Id("niveau"));
                //exp = webDriver.FindElement
                //        (By.CssSelector("#blocPersonnage > div:nth-child(9) > div.legende > span > span.js-experience"));
                //expMax = webDriver.FindElement
                //    (By.CssSelector("#blocPersonnage > div:nth-child(9) > div.legende > span > span.js-experience-max"));
                //chakra = webDriver.FindElement
                //    (By.CssSelector("#blocPersonnage > div:nth-child(13) > div.legende > span > span.js-chakra"));
                //chakraMax = webDriver.FindElement
                //    (By.CssSelector("#blocPersonnage > div:nth-child(13) > div.legende > span > span.js-chakra-max"));

                //hpLimit.Maximum = Convert.ToInt32(hpMax.Text);
                //selectedAttitude = webDriver.
                //    FindElement(By.CssSelector("#attitude js-attitude-button selected")).
                //        FindElement(By.CssSelector(".attitude-border"));
                selectedAttitude = webDriver.FindElement(By.CssSelector(".js-attitude-button.selected"))
                        .FindElement(By.CssSelector(".attitude-border"));
                if (Convert.ToInt32(hp.Text) == 0)
                      _ = ToggleOn.BeginInvoke(new InvokeDelagate( () => ToggleOn.Checked = false));
                    //ToggleOn.Checked = false;

                return true;
            }
            catch (Exception)
            {
                try
                {
                    _ = ToggleOn.BeginInvoke(new InvokeDelagate(() => ToggleOn.Checked = false));
                    //_ = ToggleOn.Invoke(new InvokeDelagate(() => ToggleOn.Checked = false));
                    //ToggleOn.Checked = false;
                }
                catch (Exception)
                {
                    return false;
                }
                return false;
            }
        }

        public void updateCharBox()
        {
            bool succes = false;
            while (!succes)
            {
                // try
                //{
                charInfoBox.Text =
                    $"{charName.Text}\n" +
                    $"HP: {hp.Text}/{hpMax.Text}\n";

                succes = true;
                //}
            }
        }

        public bool isAlertPresent()
        {
            try
            {
                webDriver.SwitchTo().Alert();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
        
        public void checkBreak()
        {
            try
            {
                webDriver.FindElement(By.CssSelector(".button-break-alteration"));
                webDriver.FindElement(By.TagName("body")).SendKeys("x");

            }
            catch (Exception)
            {

            }
        }

        public void ReEquip()
        {
            try
            {
                IReadOnlyList<IWebElement> items = webDriver.FindElements(By.CssSelector(".starred-item"));

                foreach (IWebElement item in items)
                {
                    string hotkey = item.FindElement(By.CssSelector(".shortcut")).Text;
                    if (hotkey.Contains(weaponKey.Text) || hotkey.ToLower().Contains(weaponKey.Text))
                        if (!item.GetAttribute("class").Contains("equiped"))
                            webDriver.FindElement(By.TagName("body")).SendKeys(weaponKey.Text);

                }
            }
            catch (Exception)
            {

            }
        }

        //public bool EnemyIsPresent()
        //{
        //    try
        //    {
        //        string wat;
        //        wat = webDriver.FindElement(By.CssSelector("#resultats.opponents")).Text;
        //        if (wat.Contains("Il n'y a aucun shinobi ennemi sur ce territoire."))
        //        {
        //            enemyLabel.Text = "Non";
        //            return false;
        //        }
        //        else
        //        {
        //            enemyLabel.Text = "Oui";
        //            return true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        ToggleOn.Checked = false;
        //        MessageBox.Show("Off");
        //        return false;
        //    }
        //}
        

        #region Useless

        private void loginInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void passwordInput_TextChanged(object sender, EventArgs e)
        {

        }
        private void hpLabel_Click(object sender, EventArgs e)
        {

        }

        private void botPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }


        private void botForm_Load(object sender, EventArgs e)
        {

        }

        private void charInfoBox_TextChanged(object sender, EventArgs e)
        {

        }
        private void attackButton_Click(object sender, EventArgs e)
        {
            webDriver.FindElement(By.TagName("body")).SendKeys(keysToSend.Text);
        }


        #endregion



        private void botForm_Load_1(object sender, EventArgs e)
        {

        }

        private void programClose(object sender, FormClosingEventArgs e)
        {
            try
            {
                //MessageBox.Show("WTATF");
                // webDriver.Close();
                webDriver.Quit();
                this.Dispose(true);
            }
            catch(Exception)
            {
                //webDriver.Quit();
                this.Dispose(true);
            }
        }

        private void minValChange(object sender, EventArgs e)
        {
            if (minDelayNum.Value > maxDelayNum.Value)
            {
                minDelayNum.Value = maxDelayNum.Value - 1;
            }
        }

        private void maxValueChange(object sender, EventArgs e)
        {
            if (maxDelayNum.Value < minDelayNum.Value)
                maxDelayNum.Value = minDelayNum.Value + 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try {
                MessageBox.Show(webDriver.
                       FindElement(By.CssSelector(".js-attitude-button.selected"))
                        .FindElement(By.CssSelector(".attitude-border")).Text);


            }
            catch (Exception)
            {
                MessageBox.Show("nothing");
            }
        }
    }
}
