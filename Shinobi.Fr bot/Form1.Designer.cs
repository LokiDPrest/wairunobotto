﻿using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Support.UI;

namespace Shinobi.Fr_bot
{
    partial class botForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param
        /// 
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                webDriver.Quit();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(botForm));
            this.charInfoBox = new System.Windows.Forms.RichTextBox();
            this.hpLabel = new System.Windows.Forms.Label();
            this.hpLimit = new System.Windows.Forms.NumericUpDown();
            this.loginPanel = new System.Windows.Forms.Panel();
            this.loginButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.passwordInput = new System.Windows.Forms.TextBox();
            this.loginInput = new System.Windows.Forms.TextBox();
            this.botPanel = new System.Windows.Forms.Panel();
            this.weaponKey = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.keysToSend2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.hpLimit2 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.minDelayNum = new System.Windows.Forms.NumericUpDown();
            this.maxDelayNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.keysToSend = new System.Windows.Forms.TextBox();
            this.ToggleOn = new System.Windows.Forms.CheckBox();
            this.fuyardToggle = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.KeyToSend3 = new System.Windows.Forms.TextBox();
            this.HpLimit3 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.KeyToSend4 = new System.Windows.Forms.TextBox();
            this.HpLimit4 = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.hpLimit)).BeginInit();
            this.loginPanel.SuspendLayout();
            this.botPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hpLimit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minDelayNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxDelayNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HpLimit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HpLimit4)).BeginInit();
            this.SuspendLayout();
            // 
            // charInfoBox
            // 
            this.charInfoBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.charInfoBox.Location = new System.Drawing.Point(47, 47);
            this.charInfoBox.Name = "charInfoBox";
            this.charInfoBox.ReadOnly = true;
            this.charInfoBox.Size = new System.Drawing.Size(201, 197);
            this.charInfoBox.TabIndex = 2;
            this.charInfoBox.Text = "";
            this.charInfoBox.TextChanged += new System.EventHandler(this.charInfoBox_TextChanged);
            // 
            // hpLabel
            // 
            this.hpLabel.AutoSize = true;
            this.hpLabel.Location = new System.Drawing.Point(287, 50);
            this.hpLabel.Name = "hpLabel";
            this.hpLabel.Size = new System.Drawing.Size(92, 25);
            this.hpLabel.TabIndex = 4;
            this.hpLabel.Text = "HP Limit";
            this.hpLabel.Click += new System.EventHandler(this.hpLabel_Click);
            // 
            // hpLimit
            // 
            this.hpLimit.Location = new System.Drawing.Point(301, 78);
            this.hpLimit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.hpLimit.Name = "hpLimit";
            this.hpLimit.Size = new System.Drawing.Size(91, 31);
            this.hpLimit.TabIndex = 5;
            this.hpLimit.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.hpLimit.ValueChanged += new System.EventHandler(this.hpLimitNums_ValueChanged);
            // 
            // loginPanel
            // 
            this.loginPanel.Controls.Add(this.loginButton);
            this.loginPanel.Controls.Add(this.label2);
            this.loginPanel.Controls.Add(this.label1);
            this.loginPanel.Controls.Add(this.passwordInput);
            this.loginPanel.Controls.Add(this.loginInput);
            this.loginPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginPanel.Location = new System.Drawing.Point(0, 0);
            this.loginPanel.Name = "loginPanel";
            this.loginPanel.Size = new System.Drawing.Size(884, 455);
            this.loginPanel.TabIndex = 6;
            // 
            // loginButton
            // 
            this.loginButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.loginButton.Location = new System.Drawing.Point(392, 216);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(86, 36);
            this.loginButton.TabIndex = 5;
            this.loginButton.Text = "Go";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(382, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(420, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "ID";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // passwordInput
            // 
            this.passwordInput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.passwordInput.Location = new System.Drawing.Point(312, 165);
            this.passwordInput.Name = "passwordInput";
            this.passwordInput.PasswordChar = '*';
            this.passwordInput.Size = new System.Drawing.Size(247, 31);
            this.passwordInput.TabIndex = 1;
            // 
            // loginInput
            // 
            this.loginInput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.loginInput.Location = new System.Drawing.Point(312, 91);
            this.loginInput.Name = "loginInput";
            this.loginInput.Size = new System.Drawing.Size(247, 31);
            this.loginInput.TabIndex = 0;
            // 
            // botPanel
            // 
            this.botPanel.BackgroundImage = global::Shinobi.Fr_bot.Properties.Resources.Sasuke;
            this.botPanel.Controls.Add(this.label11);
            this.botPanel.Controls.Add(this.KeyToSend4);
            this.botPanel.Controls.Add(this.HpLimit4);
            this.botPanel.Controls.Add(this.label12);
            this.botPanel.Controls.Add(this.label9);
            this.botPanel.Controls.Add(this.KeyToSend3);
            this.botPanel.Controls.Add(this.HpLimit3);
            this.botPanel.Controls.Add(this.label10);
            this.botPanel.Controls.Add(this.fuyardToggle);
            this.botPanel.Controls.Add(this.weaponKey);
            this.botPanel.Controls.Add(this.label8);
            this.botPanel.Controls.Add(this.button1);
            this.botPanel.Controls.Add(this.keysToSend2);
            this.botPanel.Controls.Add(this.label7);
            this.botPanel.Controls.Add(this.hpLimit2);
            this.botPanel.Controls.Add(this.label6);
            this.botPanel.Controls.Add(this.label5);
            this.botPanel.Controls.Add(this.label4);
            this.botPanel.Controls.Add(this.minDelayNum);
            this.botPanel.Controls.Add(this.maxDelayNum);
            this.botPanel.Controls.Add(this.label3);
            this.botPanel.Controls.Add(this.keysToSend);
            this.botPanel.Controls.Add(this.ToggleOn);
            this.botPanel.Controls.Add(this.hpLimit);
            this.botPanel.Controls.Add(this.charInfoBox);
            this.botPanel.Controls.Add(this.hpLabel);
            this.botPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.botPanel.Location = new System.Drawing.Point(0, 0);
            this.botPanel.Name = "botPanel";
            this.botPanel.Size = new System.Drawing.Size(884, 455);
            this.botPanel.TabIndex = 3;
            this.botPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.botPanel_Paint);
            // 
            // weaponKey
            // 
            this.weaponKey.Location = new System.Drawing.Point(714, 269);
            this.weaponKey.MaxLength = 10;
            this.weaponKey.Name = "weaponKey";
            this.weaponKey.Size = new System.Drawing.Size(91, 31);
            this.weaponKey.TabIndex = 19;
            this.weaponKey.Text = "f";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(709, 227);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 25);
            this.label8.TabIndex = 18;
            this.label8.Text = "KeyArme";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(597, 382);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(225, 37);
            this.button1.TabIndex = 17;
            this.button1.Text = "GetWail\'sCreditCard";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // keysToSend2
            // 
            this.keysToSend2.Location = new System.Drawing.Point(455, 165);
            this.keysToSend2.MaxLength = 10;
            this.keysToSend2.Name = "keysToSend2";
            this.keysToSend2.Size = new System.Drawing.Size(91, 31);
            this.keysToSend2.TabIndex = 16;
            this.keysToSend2.Text = "s";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(450, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 25);
            this.label7.TabIndex = 15;
            this.label7.Text = "KeyGellule:";
            // 
            // hpLimit2
            // 
            this.hpLimit2.Location = new System.Drawing.Point(455, 78);
            this.hpLimit2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.hpLimit2.Name = "hpLimit2";
            this.hpLimit2.Size = new System.Drawing.Size(91, 31);
            this.hpLimit2.TabIndex = 14;
            this.hpLimit2.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(450, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 25);
            this.label6.TabIndex = 13;
            this.label6.Text = "HP Min.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(168, 288);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 25);
            this.label5.TabIndex = 12;
            this.label5.Text = "Max. Delay";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 25);
            this.label4.TabIndex = 11;
            this.label4.Text = "Min. Delay.";
            // 
            // minDelayNum
            // 
            this.minDelayNum.Location = new System.Drawing.Point(47, 328);
            this.minDelayNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.minDelayNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.minDelayNum.Name = "minDelayNum";
            this.minDelayNum.Size = new System.Drawing.Size(91, 31);
            this.minDelayNum.TabIndex = 10;
            this.minDelayNum.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.minDelayNum.ValueChanged += new System.EventHandler(this.minValChange);
            // 
            // maxDelayNum
            // 
            this.maxDelayNum.Location = new System.Drawing.Point(188, 328);
            this.maxDelayNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.maxDelayNum.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.maxDelayNum.Name = "maxDelayNum";
            this.maxDelayNum.Size = new System.Drawing.Size(91, 31);
            this.maxDelayNum.TabIndex = 9;
            this.maxDelayNum.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.maxDelayNum.ValueChanged += new System.EventHandler(this.maxValueChange);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "KeyPotion:";
            // 
            // keysToSend
            // 
            this.keysToSend.Location = new System.Drawing.Point(301, 165);
            this.keysToSend.MaxLength = 10;
            this.keysToSend.Name = "keysToSend";
            this.keysToSend.Size = new System.Drawing.Size(91, 31);
            this.keysToSend.TabIndex = 7;
            this.keysToSend.Text = "d";
            // 
            // ToggleOn
            // 
            this.ToggleOn.AutoSize = true;
            this.ToggleOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToggleOn.Location = new System.Drawing.Point(47, 390);
            this.ToggleOn.Name = "ToggleOn";
            this.ToggleOn.Size = new System.Drawing.Size(84, 29);
            this.ToggleOn.TabIndex = 6;
            this.ToggleOn.Text = "Start";
            this.ToggleOn.UseVisualStyleBackColor = true;
            this.ToggleOn.Click += new System.EventHandler(this.toggleOn_Click);
            // 
            // fuyardToggle
            // 
            this.fuyardToggle.AutoSize = true;
            this.fuyardToggle.Location = new System.Drawing.Point(653, 328);
            this.fuyardToggle.Name = "fuyardToggle";
            this.fuyardToggle.Size = new System.Drawing.Size(155, 29);
            this.fuyardToggle.TabIndex = 20;
            this.fuyardToggle.Text = "Anti-Fuyard";
            this.fuyardToggle.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(592, 133);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 25);
            this.label9.TabIndex = 24;
            this.label9.Text = "KeyAutre:";
            // 
            // KeyToSend3
            // 
            this.KeyToSend3.Location = new System.Drawing.Point(606, 165);
            this.KeyToSend3.MaxLength = 10;
            this.KeyToSend3.Name = "KeyToSend3";
            this.KeyToSend3.Size = new System.Drawing.Size(91, 31);
            this.KeyToSend3.TabIndex = 23;
            this.KeyToSend3.Text = "d";
            // 
            // HpLimit3
            // 
            this.HpLimit3.Location = new System.Drawing.Point(606, 78);
            this.HpLimit3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.HpLimit3.Name = "HpLimit3";
            this.HpLimit3.Size = new System.Drawing.Size(91, 31);
            this.HpLimit3.TabIndex = 22;
            this.HpLimit3.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(592, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 25);
            this.label10.TabIndex = 21;
            this.label10.Text = "HP Limit";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(716, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 25);
            this.label11.TabIndex = 28;
            this.label11.Text = "KeyAutre:";
            // 
            // KeyToSend4
            // 
            this.KeyToSend4.Location = new System.Drawing.Point(730, 165);
            this.KeyToSend4.MaxLength = 10;
            this.KeyToSend4.Name = "KeyToSend4";
            this.KeyToSend4.Size = new System.Drawing.Size(91, 31);
            this.KeyToSend4.TabIndex = 27;
            this.KeyToSend4.Text = "d";
            // 
            // HpLimit4
            // 
            this.HpLimit4.Location = new System.Drawing.Point(730, 78);
            this.HpLimit4.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.HpLimit4.Name = "HpLimit4";
            this.HpLimit4.Size = new System.Drawing.Size(91, 31);
            this.HpLimit4.TabIndex = 26;
            this.HpLimit4.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(716, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 25);
            this.label12.TabIndex = 25;
            this.label12.Text = "HP Limit";
            // 
            // botForm
            // 
            this.AcceptButton = this.loginButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(884, 455);
            this.Controls.Add(this.botPanel);
            this.Controls.Add(this.loginPanel);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "botForm";
            this.Text = "WairuNoBotto";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.programClose);
            this.Load += new System.EventHandler(this.botForm_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.hpLimit)).EndInit();
            this.loginPanel.ResumeLayout(false);
            this.loginPanel.PerformLayout();
            this.botPanel.ResumeLayout(false);
            this.botPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hpLimit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minDelayNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxDelayNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HpLimit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HpLimit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        IWebDriver webDriver;
        IWebElement charName;
        IWebElement hp;
        IWebElement hpMax;
        IWebElement selectedAttitude;
        private System.Windows.Forms.RichTextBox charInfoBox;
        private System.Windows.Forms.Label hpLabel;
        private System.Windows.Forms.NumericUpDown hpLimit;
        private System.Windows.Forms.Panel loginPanel;
        private System.Windows.Forms.TextBox loginInput;
        private System.Windows.Forms.TextBox passwordInput;
        private System.Windows.Forms.Panel botPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.CheckBox ToggleOn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox keysToSend;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown minDelayNum;
        private System.Windows.Forms.NumericUpDown maxDelayNum;
        private System.Windows.Forms.TextBox keysToSend2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown hpLimit2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox weaponKey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox fuyardToggle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox KeyToSend3;
        private System.Windows.Forms.NumericUpDown HpLimit3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox KeyToSend4;
        private System.Windows.Forms.NumericUpDown HpLimit4;
        private System.Windows.Forms.Label label12;
    }
}

